package activitatDos3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Client {

	String nif;
	String nom;
	int total_facturacio;  //un enter, amb la quantitat de diners gastada pel client
	String telefon;
	String correu;
	List<Comanda> comandes = new ArrayList<Comanda>();
	
	public Client() {
		
	}
	
	/**
	 * @param nif
	 * @param nom
	 * @param total_facturacio
	 */
	public Client(String nif, String nom, int total_facturacio) {
		super();
		this.nif = nif;
		this.nom = nom;
		this.total_facturacio = total_facturacio;
	}
	
	public Client(boolean demanarDades) {
		
		if (demanarDades) {
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Introdueixi el nif: ");
			nif = sc.next();

			System.out.print("Introdueixi el nom: ");
			sc.nextLine();
			nom = sc.nextLine();
			
			boolean facturacioCorrecte = false;
			while(facturacioCorrecte == false) {
				System.out.print("Introdueixi la facturacio total: ");
				String facturacioStr = sc.next();
				
				//Si dona un error al convertir el String en int, no sortira del bucle, i per tant, es tornara a executar
				try {
					total_facturacio = Integer.parseInt(facturacioStr);
					
					if (total_facturacio > -1) {
						facturacioCorrecte = true;
					}
				}catch (Exception e) {
					System.out.println("La facturacio ha de ser un numero positiu");
				}		
			}
			
			boolean telefonCorrecte = false;
			
			sc.nextLine();
			while(telefonCorrecte == false) {
				System.out.print("Introdueixi el numero de telefon: ");
				String telefonStr = sc.nextLine();
				
				//Com es un camp opcional, comprobo si el usuari ha introduit algo
				if (telefonStr.equals("")) {
					telefonCorrecte = true;
				}else {
					//Si dona un error al convertir el String en int, no sortira del bucle, i per tant, es tornara a executar
					try {
						int telefonInt = Integer.parseInt(telefonStr);
						
						if (telefonInt > 0) {
							telefon = telefonStr;
							telefonCorrecte = true;
						}
					}catch (Exception e) {
						System.out.println("El numero de telefon ha de ser un numero positiu");
					}		
				}		
			}
			
			System.out.print("Introdueixi el correu electronic: ");
			String correuStr = sc.nextLine();
			
			//Com es un camp opcional, comprobo si el usuari ha introduit algo
			if (!correuStr.equals("")) {
				correu = correuStr;
			}
		}	
	}

	@Override
	public String toString() {
		return "Client: nif=" + nif + ", nom=" + nom + ", total_facturacio=" + total_facturacio + ", telefon=" + telefon
				+ ", correu=" + correu + "]: \nComandes:\n" + comandes.toString();
	}
}
