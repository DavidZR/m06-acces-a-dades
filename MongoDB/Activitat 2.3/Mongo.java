package activitatDos3;


import static com.mongodb.client.model.Filters.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.bson.Document;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;



public class Mongo {
	
	//Exercici 1
	public static void insereixClient(Client client) {
		try(MongoClient mongoClient = MongoClients.create();)
		// MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017");
		{
			MongoDatabase database = mongoClient.getDatabase("bd_clients");
			MongoCollection<Document> collection = database.getCollection("clients");
			Document doc = new Document("nif", client.nif).append("nom", client.nom).append("total_facturacio", client.total_facturacio);
			collection.insertOne(doc);
		}
	}
	
	//Exercici 2
	public static void insereixClient2(Client client) {
		
		//Primer comprobem si el client ya exsisteix
		List<Client> llistaClients = getAllClients();
        boolean clientExistent = false;
        for(Client c : llistaClients){
            if (c.nif.equals(client.nif)){
                clientExistent = true;
                break;
            }
        } 
		
        if (clientExistent) {
			System.out.println("El client ya existeix en la base de dades");
			
        }else {
        	try(MongoClient mongoClient = MongoClients.create();)
			// MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017");
			{
				MongoDatabase database = mongoClient.getDatabase("bd_clients");
				MongoCollection<Document> collection = database.getCollection("clients");
				Document doc = new Document("nif", client.nif).append("nom", client.nom).append("total_facturacio", client.total_facturacio);
				
				
				//Comprobem si els camps opcionals tenen valors. En cas afirmatiu, els afegim al document. 
				if (client.telefon != null) {
					doc.append("telefon", client.telefon);
				}
				
				if(client.correu != null) {
					doc.append("correu", client.correu);
				}
				
				collection.insertOne(doc);
			}
        }
	}
	
	// Versio del inserexi on tambe s'afegeixen les comandes
	public static void insereixClient3(Client client) {
		
		//Primer comprobem si el client ya exsisteix
		List<Client> llistaClients = getAllClients();
        boolean clientExistent = false;
        for(Client c : llistaClients){
            if (c.nif.equals(client.nif)){
                clientExistent = true;
                break;
            }
        } 
		
        if (clientExistent) {
			System.out.println("El client ya existeix en la base de dades");
			
        }else {
        	try(MongoClient mongoClient = MongoClients.create();)
			// MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017");
			{
				MongoDatabase database = mongoClient.getDatabase("bd_clients");
				MongoCollection<Document> collection = database.getCollection("clients");
				Document doc = new Document("nif", client.nif).append("nom", client.nom).append("total_facturacio", client.total_facturacio);
				
				
				//Comprobem si els camps opcionals tenen valors. En cas afirmatiu, els afegim al document. 
				if (client.telefon != null) {
					doc.append("telefon", client.telefon);
				}
				
				if(client.correu != null) {
					doc.append("correu", client.correu);
				}
				
				// Si la llista comandas esta inicialitzada, es a dir, te valors, els guardem al document   
				if (client.comandes != null){
                    List<Document> llistaComandes = new ArrayList<>();
                    for (Comanda comanda : client.comandes){
                    	llistaComandes.add(new Document("data_comanda", comanda.data_comanda).append("import", comanda.importe).append("pagada", comanda.pagada));
                    }

                    doc.append("comandes", llistaComandes);
                }
				
				collection.insertOne(doc);
			}
        }
	}
	
	
	//Exercici 3
	public static void getAllNif() {
		try(MongoClient mongoClient = MongoClients.create();)
	    {
	        MongoDatabase database = mongoClient.getDatabase("bd_clients");
	        MongoCollection<Document> collection = database.getCollection("clients");
	        
	        try (MongoCursor<Document> cursor = collection.find().cursor();) {
	        	// Per cada bucle, treballem amb un client de la taula
	        	int i = 0;
	            while (cursor.hasNext()) {
	            	Document d = cursor.next();
	            	i++;
	            	System.out.println(i + ") " + d.getString("nif"));
	            }
	        }
	    }
	}
	
	//Exercici 4
	public static void insereixComanda() {
		List<Client> llistaClients = getAllClients();
		
		//Comprovem si hi ha algun client
		if (llistaClients.size() == 0) {
			System.out.println("No hi ha clients a la bbdd");
		}else {
			Scanner sc = new Scanner(System.in);
			
			//Mostrem els nif de tots esl clients
			for (int i = 0; i < llistaClients.size(); i++) {
				System.out.println((i + 1) + ") " + llistaClients.get(i).nif);
			}
			
			System.out.print("");
			System.out.print("Seleccioni una client: ");
			int opcio = sc.nextInt();
			
			//Comprovem que s'ha seleccionat una opcio valida
			if (opcio < 1 || opcio > llistaClients.size()) {
				System.out.println("No ha introduit una opcio valida");
			}else {
				int index = opcio - 1;
				
				//Si el client no te cap comanda, inicialitzm la variable  
				/*if (llistaClients.get(index).comandes == null) {
					llistaClients.get(index).comandes = new ArrayList<Comanda>();
				}*/
				
				//Demanem la comanda i la afegim a la llista "comandes" del client
				Comanda comanda = new Comanda(true);
				llistaClients.get(index).comandes.add(comanda);
				
				//Incrementem la facturacio total del client sumantli el import de la nova comanda
				llistaClients.get(index).total_facturacio += comanda.importe;
				
				//Actualitzem les dades del client
				//Tot i que existeixen mètodes d'actualització de documents, nosaltres, per simplificar, esborrarem el document i 
				//l'afegirem de nou amb les dades modificades.
				deleteClient(llistaClients.get(index));
	            insereixClient3(llistaClients.get(index));

			}
		}
	}
	
	
	//Exercici 5
	public static void llegirClientsByFacturacio(int facturacioMinima) {
		try(MongoClient mongoClient = MongoClients.create();)
	    {
	        MongoDatabase database = mongoClient.getDatabase("bd_clients");
	        MongoCollection<Document> collection= database.getCollection("clients");
	        
	        try (MongoCursor<Document> cursor = collection.find(gt("total_facturacio", facturacioMinima)).iterator();) {
	        	// Per cada bucle, treballem amb un client de la taula
	            while (cursor.hasNext()) {
	            	Client client = new Client();
	                Document doc = cursor.next();
	                client.nif = doc.getString("nif");
	                client.nom = doc.getString("nom");
	                client.total_facturacio = doc.getInteger("total_facturacio");
	                
	                // Si el camp opcional te un valor, el guardem en la variable del objecte
	                if (doc.getInteger("telefono") != null){
	                    client.telefon = doc.getString("telefon");
	                }
	                
	                if (doc.getString("email") != null){
	                    client.correu = doc.getString("correu");
	                }
	
	                // Si el client te alguna comanda, les guardem en la llista comandes del objecte client
	                if (doc.getList("comandes", Document.class) != null){
	
	                	List<Comanda> comandes = new ArrayList<>();
	                	
	                    List<Document> llistaComandes = doc.getList("comandes", Document.class);
	                    for (Document subdoc : llistaComandes) {
	                        Comanda comanda = new Comanda(subdoc.getDate("data_comanda"),subdoc.getInteger("import"), subdoc.getBoolean("pagada"));
	                       comandes.add(comanda);
	                    }
	                    client.comandes = comandes;
	                }
	                System.out.println(client.toString());
	            }  
	        }
	    }
	}
	
	//Exercici 6
	public static void llegirClientsByComandes(int comandesMinimes) {
		List<Client> llistaClients = getAllClients();
        boolean clientExistent = false;
        for(Client c : llistaClients){
    		if (c.comandes.size() > comandesMinimes){
                System.out.println(c);
	            
        	}
        	
        } 
	}
	// Funcio per treure tots els clients amb totes les seves comandes de la bd 
	public static List<Client> getAllClients(){
	    List<Client> resultat = new ArrayList<>();
	    
	    try(MongoClient mongoClient = MongoClients.create();)
	    {
	        MongoDatabase database = mongoClient.getDatabase("bd_clients");
	        MongoCollection<Document> collection= database.getCollection("clients");
	        
	        try (MongoCursor<Document> cursor = collection.find().cursor();) {
	        	
	        	// Per cada bucle, treballem amb un client de la taula
	            while (cursor.hasNext()) {
	                Client client = new Client();
	                Document doc = cursor.next();
	                client.nif = doc.getString("nif");
	                client.nom = doc.getString("nom");
	                client.total_facturacio = doc.getInteger("total_facturacio");
	
	                // Si el camp opcional te un valor, el guardem en la variable del objecte
	                if (doc.getInteger("telefono") != null){
	                    client.telefon = doc.getString("telefon");
	                }
	                
	                if (doc.getString("email") != null){
	                    client.correu = doc.getString("correu");
	                }
	
	                
	                // Si el client te alguna comanda, les guardem en la llista comandes del objecte client
	                if (doc.getList("comandes", Document.class) != null){
	                	
	                	List<Comanda> comandes = new ArrayList<>();
	                	
	                    List<Document> llistaComandes = doc.getList("comandes", Document.class);
	                    for (Document subdoc : llistaComandes) {
	                        Comanda comanda = new Comanda(subdoc.getDate("data_comanda"),subdoc.getInteger("import"), subdoc.getBoolean("pagada"));
	                       comandes.add(comanda);
	                    }
	                    client.comandes = comandes;
	                }
	                resultat.add(client);
	            }
	        }
	    }
	    return resultat;
	}
	
	//Funcio per eliminar un client concret
	public static void deleteClient(Client client){
        try (MongoClient mongoClient = MongoClients.create();){
            MongoDatabase database = mongoClient.getDatabase("bd_clients");
            MongoCollection<Document> collection = database.getCollection("clients");
            DeleteResult deleteResult = collection.deleteOne(new Document("nif", client.nif));
        }
    }
}
