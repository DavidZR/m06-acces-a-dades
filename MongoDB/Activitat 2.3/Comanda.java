package activitatDos3;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class Comanda {

	Date data_comanda;
	int importe;
	boolean pagada;

	/**
	 * @param data_comanda
	 * @param importe
	 * @param pagada
	 */
	public Comanda(Date data_comanda, int importe, boolean pagada) {
		super();
		this.data_comanda = data_comanda;
		this.importe = importe;
		this.pagada = pagada;
	}
	
	
	public Comanda(boolean demanarDades) {
		if (demanarDades) {
			Scanner sc = new Scanner(System.in);
			
			System.out.print("Introdueixi la data de la comanda (01/01/2001): ");
			String data = sc.next();
			
			try {
				data_comanda = java.sql.Date.valueOf(LocalDate.parse(data, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			}catch (Exception e){
				System.out.println("La data que has introduit es incorrecte");
			}
			
			boolean importCorrecte = false;
			
			sc.nextLine();
			while(importCorrecte == false) {
				System.out.print("Introdueixi el import de la comanda: ");
				String importStr = sc.nextLine();
				
				//Si dona un error al convertir el String en int, no sortira del bucle, i per tant, es tornara a executar
				try {
					 importe = Integer.parseInt(importStr);
					
					if (importe > 0) {
						importCorrecte = true;
					}
				}catch (Exception e) {
					System.out.println("El import ha de ser un numero positiu");	
				}		
			}

			boolean valorCorrecte = false;
            while (!valorCorrecte) {
                System.out.println("La comanda esta pagada? (s|n): ");
                String valor = sc.nextLine();
                
                //Comprovem que han introduit 's' o 'n'
                if (valor.equals("s") || valor.equals("n")) {

                    if (valor.equals("s")){
                        pagada = true;
                    } else {
                        pagada = false;
                    }
                    valorCorrecte = true;
                }
            }
		}
	}


	@Override
	public String toString() {
		return "Comanda [data_comanda=" + data_comanda + ", importe=" + importe + ", pagada=" + pagada + "] \n";
	}
	
	
}
