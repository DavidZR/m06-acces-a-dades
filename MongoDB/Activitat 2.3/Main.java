package activitatDos3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void exercici1() {
		Client client = new Client("12345678X", "Pere Pons", 0);
		Mongo.insereixClient(client);
	}
	
	public static void exercici2() {
		Client client = new Client(true);
		Mongo.insereixClient2(client);
	}
	
	public static void exercici3() {
		Mongo.getAllNif();
	}
	
	public static void exercici4() {
		Mongo.insereixComanda();
	}
	
	public static void exercici5() {
		Mongo.llegirClientsByFacturacio(10);
	}
	
	public static void exercici6() {
		Mongo.llegirClientsByComandes(1);
	}	
	
	public static int menu() {
		Scanner sc = new Scanner(System.in);
        System.out.println("Opcions:");
        System.out.println("0) Sortir");
        System.out.println("1) Donar d'alta un client");
        System.out.println("2) Afegir comandes a un client");
        System.out.println("3) Cercar clients per facturació");
        System.out.println("4) Cercar clients per quantitat de comandes");
        System.out.println("");
        
        System.out.print("Introdueixi una opcio: ");
        return sc.nextInt();
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		boolean sortida = false;
		while(sortida == false) {
			switch (menu()) {
			
			case 0:
				sortida = true;
				break;
			case 1:
				Client client = new Client(true);
				Mongo.insereixClient3(client);
				break;
			case 2:
				exercici4();
				break;
			case 3:
				exercici5();
				break;
			case 4:
				exercici6();
				break;
			default:
                System.out.println("Ha introduit una opcio no valida");
                break;
				
			}
		}
	}

}
