/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package activitatTres2;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author PC
 */
@Entity
@Table(name="comanda")
public class Comanda implements Serializable{
    @Id
    int num_comanda;
    double preu_total;
    Date data;
    int dni_client;

    /**
    * @param num_comanda
    * @param preu_total
    * @param data
    */
    
    public Comanda(int num_comanda, double preu_total, Date data, int dni_client) {
        super();
        this.num_comanda = num_comanda;
        this.preu_total = preu_total;
        this.data = data;
        this.dni_client = dni_client;
    }

	
    public Comanda() {

    }
        
}
