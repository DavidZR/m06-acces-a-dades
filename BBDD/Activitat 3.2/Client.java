/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package activitatTres2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author PC
 */

@Entity
@Table(name="client")
public class Client implements Serializable{
    @Id
    int dni;
    String nom;
    boolean esPremium;
    @OneToMany
    @JoinColumn(name="dni_client")
    List<Comanda> llistaComandas = new ArrayList<>();


    /**
    * @param dni
    * @param nom
    * @param premium
    */
    public Client(int dni, String nom, boolean esPremium) {
        super();
        this.dni = dni;
        this.nom = nom;
        this.esPremium = esPremium;
    }

    public Client() {

    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setEsPremium(boolean esPremium) {
        this.esPremium = esPremium;
    }
    
    
    
	
}
