/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package activitatTres2;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import java.text.ParseException;


/**
 *
 * @author PC
 */
public class Opciones {
    
    //Opcio 4
    public static void insereixNouClient() {
        Client cliente = demanarClient();
        
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            session.beginTransaction();
            session.save(cliente);
            session.getTransaction().commit();
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }
    
    public static Client demanarClient(){
        int dni = -1;
        String nom;
        String esPremiumString;
        boolean esPremium = false;

        Scanner sc = new Scanner(System.in);
        //Para poder introducir Double con comas
        sc.useLocale(Locale.ENGLISH);
        boolean salir = false;
	while (salir == false) {
            boolean dniCorrecte = true;
            System.out.print("Introdueixi el DNI del client (sense lletra): ");
            dni = sc.nextInt();
            
            Configuration configuration = new Configuration();
            configuration.configure();
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
            Session session = factory.openSession();
            
            try {
                Query query = session.createQuery("from Client");
                List<Client> listClients = query.list();
                for (Client c : listClients) {
                    if (dni == c.dni){
                        dniCorrecte = false;
                        System.out.println("No pot haber dos DNI's iguals");
                    }
                }
            }
            finally {
                session.close();
                factory.close();
                StandardServiceRegistryBuilder.destroy(serviceRegistry);
            }
                    
            //Si no hi ha cap dni igual a la bbdd sortim del bucle
            if (dniCorrecte == true){
                salir = true;
            }
        }
        
        System.out.print("Introdueixi el nom del client: ");
        nom = sc.next();

        boolean dadaCorrecta = false;
        while (dadaCorrecta == false) {
            System.out.print("Introdueixi si el client es premium o no (Si|No): ");
            esPremiumString = sc.next();

            if (esPremiumString.equals("Si")) {
                esPremium = true;
                dadaCorrecta = true;
            } else if (esPremiumString.equals("No")) {
                esPremium = false;
                dadaCorrecta = true;
            } else {
                System.out.println("No ha introduit una resposta valida");
            }
        }

        Client client = new Client(dni, nom, esPremium);
        return client;
    }
    
    /*--------------------------------------------------------------------------------*/
    //Opcio 5
    public static void insereixNovaComanda() {
        Comanda comanda = demanarComanda();
        
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            session.beginTransaction();
            session.save(comanda);
            session.getTransaction().commit();
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }
    
    public static Comanda demanarComanda(){
        int num_comanda = -1;
        double preu_total ;
        String dataString;
        Client client;
                   
        Scanner sc = new Scanner(System.in);
        //Para poder introducir Double con comas
        sc.useLocale(Locale.ENGLISH);
        
        client = triarClient();
        
        //Demano el numero de comanda
        boolean salir = false;
	while (salir == false) {
            boolean num_comandaCorrecte = true;
            System.out.print("Introdueixi el numero de la comanda: ");
            num_comanda = sc.nextInt();
            
            Configuration configuration = new Configuration();
            configuration.configure();
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
            Session session = factory.openSession();
            
            try {
                Query query = session.createQuery("from Comanda");
                List<Comanda> listComanda = query.list();
                for (Comanda co : listComanda) {
                    if (num_comanda == co.num_comanda){
                        num_comandaCorrecte = false;
                        System.out.println("No pot haber dos nuemros de comanda iguals");
                    }
                }
            }
            finally {
                session.close();
                factory.close();
                StandardServiceRegistryBuilder.destroy(serviceRegistry);
            }
                    
            
            if (num_comandaCorrecte == true){
                salir = true;
            }
        }
        
        //Demano el preu total
        System.out.print("Introdueixi el preu total de la comanda: ");
        preu_total = sc.nextDouble();
        
        // Demano la data
        System.out.print("Introdueixi la data de la comanda (01/01/2001): ");
        dataString = sc.next();
        
        // Data en sql
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date data = null;

        try {
            java.util.Date dataUtil = format.parse(dataString);
            data = new java.sql.Date(dataUtil.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        Comanda comanda = new Comanda(num_comanda, preu_total, data, client.dni);
        return comanda;
    }
    
    public static Client triarClient() {
        int numClient = 0;
        Client client = new Client();
        boolean salir = false;
        int i = -1;
        Scanner sc = new Scanner(System.in);
        
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            Query query= session.createQuery("from Client");
            List<Client> listClients = query.list();
            
            //Pido el cliente
            while(salir == false){
                //Reseteo el vlor de la variable
                i = 0;
                for (Client c : listClients) {
                    i++;
                    System.out.print(i + ": ");
                    System.out.println(c.nom + " : " + c.dni);
                }

                System.out.println("Seleciona un Client:");
                numClient = sc.nextInt();

                if (numClient < 1 || numClient > i){
                    System.out.println("El cliente que ha introducido no existe");
                }else{
                    salir = true;
                }
            }
            
            //Guardo el dni del client
            int x = 0;
            for (Client c : listClients){
                x++;
                if (x == numClient){
                    client = c;
                }
            }   
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
        return client;
    }
    
    
    /*---------------------------------------------------------------------------------------------*/
    //Opcio 1
    
    public static void eliminarClient() {
        Client client = triarClient();
        
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            session.beginTransaction();
            //Eliminem les comandas del client
            session.createQuery("delete from Comanda where dni_client= :dni").setParameter("dni", client.dni).executeUpdate();
            //Eliminem el client
            session.delete(client);
            session.getTransaction().commit();
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }
    
    
    /*------------------------------------------------------------------------------------------------*/
    //Opcio 2
     public static void actualitarClient() {
        Client client = demanarCanviClient();
        
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            session.beginTransaction();
            //Actualitzem les dades del client
            session.update(client);
            session.getTransaction().commit();
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }
     
    public static Client demanarCanviClient(){
        Client client = triarClient();
        String nom;
        String esPremiumString;
        boolean esPremium = false;
       
        Scanner sc = new Scanner(System.in);
        //Para poder introducir Double con comas
        sc.useLocale(Locale.ENGLISH);
	
        System.out.print("Introdueixi el nom del client: ");
        nom = sc.next();
        client.setNom(nom);
        
        boolean dadaCorrecta = false;
        while (dadaCorrecta == false) {
            System.out.print("Introdueixi si el client es premium o no (Si|No): ");
            esPremiumString = sc.next();

            if (esPremiumString.equals("Si")) {
                esPremium = true;
                dadaCorrecta = true;
            } else if (esPremiumString.equals("No")) {
                esPremium = false;
                dadaCorrecta = true;
            } else {
                System.out.println("No ha introduit una resposta valida");
            }
        }
        client.setEsPremium(esPremium);
        return client;
    } 
    
    
    /*-----------------------------------------------------------------------------------------*/
    //Opcion 3
    
    public static void printarClients(){
        String filtre;
        Scanner sc = new Scanner(System.in);
        //Para poder introducir Double con comas
        sc.useLocale(Locale.ENGLISH);
        
        System.out.println("Introdueixi el nom del client");
        filtre = sc.next();
        
        
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        try {
            Query query = session.createQuery("from Client where nom LIKE Concat('" + filtre + "','%')");
            List<Client> listClients = query.list();
            System.out.println("nom : DNI : esPremium");
            for (Client c : listClients) {
                System.out.println(c.nom + " : " + c.dni + " : " + c.esPremium);
            }
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }
    
    
    /*----------------------------------------------------------------------------------------------------------*/
    //Opcion 6
    public static void mostraraComandesClient() {
        Client client = triarClient();
        
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try {
            Query query = session.createQuery("from Comanda where dni_client= :dni").setParameter("dni", client.dni);
            List<Comanda> listComandas = query.list();
            System.out.println("Comandes de " + client.nom);
            System.out.println("Num comanda : Preu total : Data");
            for (Comanda co : listComandas) {
                System.out.println(co.num_comanda + " : " + co.preu_total + " : " + co.data);
            }
        }
        finally {
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }
    
}


    
    
    
    
  
