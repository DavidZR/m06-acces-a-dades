/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package activitatTres2;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.logging.Level;

/**
 *
 * @author PC
 */
public class Main {

    public static void menu() {
        System.out.println(" 0.- Sortir" + "\n 1.- Eliminar un client" + "\n 2.- Actualitzar dades d'un client" + "\n 3.- Cercar per nom del client"
                + "\n 4.- Alta d'un nou client" + "\n 5.- Alta d'una nova comanda" + "\n 6.- Mostrar comandes d'un client" + "\n 7.- Mostrar resum dels clients"
                + "\n Selecciona una opcio: ");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Show only warning info
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.WARNING);
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        //Para poder introducir Double con comas
        sc.useLocale(Locale.ENGLISH);

        boolean sortida = false;
        while (sortida == false) {
            menu();
            int opcio = sc.nextInt();
            switch (opcio) {
                case 0:
                    // Sortim del bucle
                    sortida = true;
                    break;
                case 1:
                    // Se selecciona el client i s'elimina el client i totes les seves comandes de la BBDD
                    Opciones.eliminarClient();
                    break;
                case 2:
                    // Se selecciona un client, i es tornen a introduir les seves dades (les que no siguin PK, és a dir, nom i premium). S'actualitza la informació a la BBDD
                    Opciones.actualitarClient();
                    break;
                case 3:
                    // Mostrar per pantalla els clients de la BBDD que el seu nom comenci pel text introduït per teclat. Per exemple, si l'usuari introdueix "Jo", es mostrarien les Joanes, els Joseps, els Jordis...
                    Opciones.printarClients();
                    break;
                case 4:
                    // Demana les dades del client (dni, nom i si és "premium" o no).
                    Opciones.insereixNouClient();
                    break;

                case 5:
                    // Demana en primer lloc les dades del client que està relacionat amb la
                    // comanda. Per fer-ho, mostra un menú numerat amb els DNI i noms dels clients.
                    // L'usuari selecciona el nom del client relacionat, introduint el número.
                    // Després es demanen les dades bàsiques de la comanda (número, preu i data).
                    Opciones.insereixNovaComanda();
                    break;

                case 6:
                    // Es demanarà en primer lloc el client, de la mateixa manera que a l'opció 5.
                    // Un cop seleccionat el client, es mostraran per pantalla totes les dades de la
                    // comanda.
                    Opciones.mostraraComandesClient();
                    break;
                case 7:
                    // Mostrar per pantalla un resum de tots els clients amb la quantitat total que han facturat, 
                    // ordenat de més facturació a menys facturació. Per exemple:
                    break;
            }
        }
    }

}
