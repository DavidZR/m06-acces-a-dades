package activitatDos2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ClientBBDD {

	Connection con;

	public ClientBBDD() throws SQLException {
		String url = "jdbc:mysql://localhost/db_client?serverTimezone=UTC"; // db_client, 3306
		String usuari = "dbuser";
		String password = "1234";
		this.con = DriverManager.getConnection(url, usuari, password);
		this.con.setAutoCommit(false);
		System.out.println("Connexi� oberta correctament.");
		
	}

	public void close() {
		try {
			con.close();
			System.out.println("Connexi� tancada correctament.");
		} catch (SQLException ex) {
			System.err.println("Error al tancar la connexi�.");
		}
	}

	public void creaTaula() throws SQLException {
		try {
			String sentenciaSQL1 = "CREATE TABLE client (" 
					+ "dni INT PRIMARY KEY," 
					+ "nom VARCHAR(100),"
					+ "esPremium ENUM('S','N')" 
					+ ")";
			Statement statement = con.createStatement();
			statement.execute(sentenciaSQL1);
			System.out.println("Taula client creada correctament.");

			String sentenciaSQL2 = "CREATE TABLE comanda (" + "num_comanda INT PRIMARY KEY," + "preu_total DOUBLE,"
					+ "data DATE," + "dni_client INT" + ")";
			statement = con.createStatement();
			statement.execute(sentenciaSQL2);
			System.out.println("Taula comanda creada correctament.");
		} catch (Exception e) {
			System.out.println("Les taules ya existeixen");
		}

	}

	public void eliminarContingutTaulas(){
		try{
			String sentenciaSQL1 = "DELETE from client";
			Statement statement = con.createStatement();
			statement.execute(sentenciaSQL1);

			String sentenciaSQL2 = "DELETE from comanda";
			statement = con.createStatement();
			statement.execute(sentenciaSQL2);

		} catch (Exception e) {
			
		}

	}

	public void insereix(List<Client> llistaClient) throws SQLException {

		Iterator<Client> iterClient = llistaClient.iterator();

		while (iterClient.hasNext()) {
			Client client = iterClient.next();

			String sentenciaSQL1 = "insert into client (dni,nom,esPremium) values (?,?,?)";
			PreparedStatement sentenciaPreparada1 = con.prepareStatement(sentenciaSQL1);
			sentenciaPreparada1.setInt(1, client.dni);
			sentenciaPreparada1.setString(2, client.nom);
			sentenciaPreparada1.setString(3, ((client.esPremium) ? "S" : "N"));
			sentenciaPreparada1.executeUpdate();
			System.out.println("Client inserit correctament.");

			Iterator<Comanda> iterComanda = client.llistaComandas.iterator();
			while (iterComanda.hasNext()) {
				Comanda comanda = iterComanda.next();

				String sentenciaSQL2 = "insert into comanda (num_comanda,preu_total,data,dni_client) values (?,?,?,?)";
				PreparedStatement sentenciaPreparada2 = con.prepareStatement(sentenciaSQL2);
				sentenciaPreparada2.setInt(1, comanda.num_comanda);
				sentenciaPreparada2.setDouble(2, comanda.preu_total);
				sentenciaPreparada2.setDate(3, comanda.data);
				sentenciaPreparada2.setInt(4, (comanda.dni_client));
				sentenciaPreparada2.executeUpdate();
				System.out.println("Comanda inserit correctament.");
			}
		}
	}
	
	public List<Client> selecciona(List<Client> llistaClients) throws SQLException {
       
        llistaClients.clear();
        //Dades de la taula client
        String sentenciaSQL = "select dni,nom,esPremium from client";
        Statement statement = con.createStatement();
        statement = con.createStatement();
        ResultSet rs = statement.executeQuery(sentenciaSQL);
        
        while (rs.next()) {
            Client client = new Client();
            client.dni = rs.getInt("dni");
            client.nom = rs.getString("nom");
            client.esPremium =  rs.getString("esPremium").equals("S");
            llistaClients.add(client);
        }
        
        //Dades de la taula comanda
        String sentenciaSQL2 = "select num_comanda,preu_total,data,dni_client from comanda";
        Statement statement2 = con.createStatement();
        statement2 = con.createStatement();
        ResultSet rs2 = statement.executeQuery(sentenciaSQL2);
        
        while(rs2.next()) {
        	Comanda comanda = new Comanda();
        	comanda.num_comanda = rs2.getInt("num_comanda");
            comanda.preu_total = rs2.getDouble("preu_total");
            comanda.data = rs2.getDate("data");
            comanda.dni_client = rs2.getInt("dni_client");
            
            for (Client client : llistaClients) {
            	if (comanda.dni_client == client.dni) {
            		client.llistaComandas.add(comanda);
            	}
            }
        }
        
        return llistaClients;
    }
}
