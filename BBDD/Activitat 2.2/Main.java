package activitatDos2;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Locale;

public class Main {

	public static void menu() {
		System.out.println(" 0.- Sortir" + "\n 1.- Crear taulas" + "\n 2.- Recuperar dades" + "\n 3.- Guardar dades"
				+ "\n 4.- Crear client" + "\n 5.- Crear comanda" + "\n 6.- Mostrar dades" + "\n 7.- Generaci� de resum de facturaci�"
				+ "\n Selecciona una opcio: ");
	}

	public static Client demanarDadesClient(List<Client> llistaClients) {
		int dni = -1;
		String nom;
		String esPremiumString;
		boolean esPremium = false;

		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.ENGLISH);

		boolean dniCorrecte = false;
		while (dniCorrecte == false) {
			System.out.print("Introdueixi el DNI del client (sense lletra): ");
			dni = sc.nextInt();

			// Comprovo si la llista esta buida perque si no, no entraria al bucle i la
			// variable dniCorrecte seria sempre false
			if (llistaClients.isEmpty()) {
				dniCorrecte = true;
			}

			for (Client client : llistaClients) {
				if (dni != client.dni) {
					dniCorrecte = true;
				} else {
					dniCorrecte = false;
					System.out.println("Ya existeix un clien amb aquest DNI");
					break;
				}

			}

		}

		System.out.print("Introdueixi el nom del client: ");
		nom = sc.next();

		boolean dadaCorrecta = false;
		while (dadaCorrecta == false) {
			System.out.print("Introdueixi si el client es premium o no del client (Si|No): ");
			esPremiumString = sc.next();

			if (esPremiumString.equals("Si")) {
				esPremium = true;
				dadaCorrecta = true;
			} else if (esPremiumString.equals("No")) {
				esPremium = false;
				dadaCorrecta = true;
			} else {
				System.out.println("No ha introduit una resposta valida");
			}
		}

		Client client = new Client(dni, nom, esPremium);
		return client;
	}

	public static List<Client> demanarDadesComanda(List<Client> llistaClients) {
		int num_comanda = -1;
		double preu_total;
		String dataString;
		int client = -1;
		Scanner sc = new Scanner(System.in);

		for (int i = 0; i < llistaClients.size(); i++) {
			System.out.println(i + ") " + llistaClients.get(i).dni + " - " + llistaClients.get(i).nom);
		}

		boolean sortida = false;
		while (sortida == false) {
			System.out.print("Seleccioneu el client de la comanda: ");
			client = sc.nextInt();

			if (client >= llistaClients.size() || client < 0) {
				System.out.println("Aquest client no existeix");
			} else {
				sortida = true;
			}
		}

		// Dades de comanda
		boolean num_comandaCorrecte = false;
		while (num_comandaCorrecte == false) {
			System.out.print("Introdueixi el numero de la comanda: ");
			num_comanda = sc.nextInt();

			for (int i = 0; i < llistaClients.size(); i++) {

				// Fem aquest if per si tots els clients tenen la seva llista de comandes buida
				// i, per tant, no entrariem al seguent bucle i la variable num_comandaCorrecte
				// seria sempre false
				if (llistaClients.get(i).llistaComandas.isEmpty()) {
					num_comandaCorrecte = true;
				}

				for (Comanda comanda : llistaClients.get(i).llistaComandas) {
					if (num_comanda != comanda.num_comanda) {
						num_comandaCorrecte = true;
					} else {
						// Si hi ha 2 numeros iguals sortim del for each
						num_comandaCorrecte = false;
						System.out.println("Ya existeix una comanda amb aquest numero");
						break;
					}
				}
				// Si hi ha 2 numeros iguals sortim del fori
				if (num_comandaCorrecte == false) {
					break;
				}
			}

		}

		System.out.print("Introdueixi el preu total de la comanda: ");
		preu_total = sc.nextDouble();

		System.out.print("Introdueixi la data de la comanda (01/01/2001): ");
		dataString = sc.next();

		// Data en sql
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		java.sql.Date data = null;

		try {
			java.util.Date dataUtil = format.parse(dataString);
			data = new java.sql.Date(dataUtil.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Comanda comanda = new Comanda(num_comanda, preu_total, data, llistaClients.get(client));

		// Afegim la comanda a la llista de comandes del client
		llistaClients.get(client).llistaComandas.add(comanda);

		return llistaClients;

	}

	public static void mostrarDades(List<Client> llistaClients) {
		int client;
		Scanner sc = new Scanner(System.in);

		for (int i = 0; i < llistaClients.size(); i++) {
			System.out.println(i + ") " + llistaClients.get(i).dni + "-" + llistaClients.get(i).nom);
		}

		System.out.print("Seleccioneu el client: ");
		client = sc.nextInt();

		System.out.println("Comandes de " + llistaClients.get(client).nom + ":");
		for (Comanda comanda : llistaClients.get(client).llistaComandas) {
			System.out.println("----------------------------");
			System.out.println("Numero: " + comanda.num_comanda);
			System.out.println("Preu total: " + comanda.preu_total);
			System.out.println("Data: " + comanda.data);
			System.out.println("DNI Client: " + comanda.dni_client);
		}

	}

	static void resumFacturacio(ClientBBDD bbdd) throws SQLException {
		int mes;
		int any;

		Scanner sc = new Scanner(System.in);
		System.out.print("Introdueixi el numero del mes: ");
		if (sc.hasNextInt()) {
			mes = sc.nextInt();
			System.out.print("Introdueixi l'any: ");
			if (sc.hasNextInt()) {
				any = sc.nextInt();
				if (mes > 0 && mes <= 12) {
					if (any > 0) {
						try {
							String sentenciaSQL = "CALL crea_resum_facturacio(" + mes + "," + any + " );";
							Statement statement = bbdd.con.createStatement();
							statement.execute(sentenciaSQL);
							System.out.println("Resum de facturacio creat correctament");
							bbdd.con.commit();
						} catch (Exception e) {
							System.out.println("ERROR: " + e);
						} finally {
							bbdd.close();
						}
					} else {
						System.out.println("ERROR: L'any introduit es menor a 0");
					}
				} else {
					System.out.println("ERROR: El mes introduit es menor que 1 o mayor que 12");
				}
			} else {
				System.out.println("ERROR: L'any introduit no es correcte");
			}
		} else {
			System.out.println("ERROR: El mes introduit no es correcte");
		}
	}

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.ENGLISH);

		List<Client> llistaClients = new ArrayList<Client>();
		ClientBBDD bbdd = new ClientBBDD();
		boolean sortida = false;
		while (sortida == false) {
			menu();
			int opcio = sc.nextInt();
			switch (opcio) {
			case 0:
				// Sortim del bucle
				bbdd.close();
				sortida = true;
				break;
			case 1:
				// Crea el model (les dues taules) dins la BBDD. Mostra un error si les taules
				// ja existien.
				bbdd.creaTaula();
				break;
			case 2:
				// Recupera totes les dades de la BBDD i les guarda en una la llista de clients
				// (amb les seves comandes).
				bbdd.selecciona(llistaClients);
				break;
			case 3:
				// Elimina totes les dades de la BBDD i guarda totes les dades que hi ha a la
				// llista de clients de l'aplicaci� a la BBDD
				bbdd.eliminarContingutTaulas();
				bbdd.insereix(llistaClients);
				break;

			case 4:
				// Demana les dades del client (dni, nom i si �s "premium" o no) i l'afegeix a
				// la llista de clients. Aquesta operaci� no emmagatzema res a la BBDD, ja que
				// l'emmagatzemat es fa a l'opci� 3.
				Client client = demanarDadesClient(llistaClients);
				llistaClients.add(client);
				break;

			case 5:
				// Demana en primer lloc les dades del client que est� relacionat amb la
				// comanda. Per fer-ho, mostra un men� numerat amb els DNI i noms dels clients.
				// L'usuari selecciona el nom del client relacionat, introduint el n�mero.
				// Despr�s es demanen les dades b�siques de la comanda (n�mero, preu i data).
				demanarDadesComanda(llistaClients);
				break;

			case 6:
				// Es demanar� en primer lloc el client, de la mateixa manera que a l'opci� 5.
				// Un cop seleccionat el client, es mostraran per pantalla totes les dades de la
				// comanda.
				mostrarDades(llistaClients);
				break;
			case 7:
				// Fes una extensi� de la pr�ctica 1.3, afegint una opci� 7 al men�. L'opci�
				// ser� "Generaci� de resum de facturaci�", que demanar� per teclat el mes i
				// l'any i que cridar� al procediment emmagatzemat que acabes de crear. Has de
				// tenir en compte:
				resumFacturacio(bbdd);

			}

		}

	}

}
