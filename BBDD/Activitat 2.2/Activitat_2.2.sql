use db_client;
CREATE TABLE resum_facturacio(
    mes INT, 
    any INT, 
    dni_client INT (8), 
    quantitat DOUBLE(30, 2), 
    PRIMARY KEY (mes, any, dni_client), 
    FOREIGN KEY (dni_client) REFERENCES client(dni));

DROP PROCEDURE IF EXISTS crea_resum_facturacio;
DELIMITER //
CREATE PROCEDURE crea_resum_facturacio
(IN p_mes INT, p_any INT)
BEGIN
DECLARE v_faturacio_total DOUBLE(5,2);
DECLARE v_dni INT;
DECLARE fin INT DEFAULT FALSE; 

DECLARE c_facturacio_total CURSOR FOR SELECT SUM(preu_total), dni_client FROM comanda WHERE MONTH(data) = p_mes AND YEAR(data) = p_any GROUP BY dni_client;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin = TRUE;

OPEN c_facturacio_total; 
read_loop: LOOP
    FETCH c_comanda INTO v_total_facturacion, v_dni;
    IF fin THEN
        LEAVE read_loop;
    END IF;
    INSERT INTO resum_facturacio (mes, any, dni_client, quantitat ) 
    VALUES (p_mes, p_any, v_dni, v_facturacio_total);
END LOOP;
CLOSE c_facturacio_total;
END; 
//

call crea_resum_facturacio(3, 2020);