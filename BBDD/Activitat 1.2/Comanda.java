package activitatUno2;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.xml.crypto.Data;

public class Comanda {

	int num_comanda;
	double preu_total;
	Date data;
	int dni_client;

	/**
	 * @param num_comanda
	 * @param preu_total
	 * @param data
	 */
	public Comanda(int num_comanda, double preu_total, Date data, Client client) {
		super();
		this.num_comanda = num_comanda;
		this.preu_total = preu_total;
		this.data = data;
		this.dni_client = client.dni;
	}

	
	public Comanda() {
		
	}
}
